# Front matter module
## Introduction
The _Front Matter_ module adds [front matter](https://jekyllrb.com/docs/front-matter/) entity updating and management to a site. With this module installed and configured you can then add and update blog posts via API requests in a relatively straightforward manner - right from your favourite terminal or editor!

## Requirements
This module currently requires the [Key Auth](https://www.drupal.org/project/key_auth) module for authentication using an API key.

## Installation
Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8

## Maintainers
Current maintainers:
- [Michael Welford](https://www.drupal.org/u/mikejw)
