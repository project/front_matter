# Front matter module todo
## Todo
- Run through complete life cycle, manual
- Image handling? See below
- Remove hardcoding of 'blog_post'
- Create settings
- Generic completion route for a field type
- Addition of [x] for tags, series etc. will use that for the id when present
- Clean up fetch to take id and title as a param
- De-hardcode all the things
- Open post on website, unpublished or published, side by side thing, live reload
- Multiple content types? Fields per type
- update schema
- update README, Vim setup stuff
- functional tests
- plugins for editors to get the autocompletion working
- a script to transmit images

## Done
- Fix vim command to reload what is sent back @done
- Allow one end point to handle both create and update @done
- square brackets to denote ids @done
- caret? next to fields e.g. size^: to denote readonly nah use bang @done

## Use [id]
why? so can make sure there is a match for a field type and signify that specific
tags are currently like: tags: thing,blah,"another thing"
also support: tags: thing [24],blah [12], "another thing" [1]
and: tags: [24],[12],[1]
...essentially the idea being that if there are [x] then it uses that, otherwise falls back to text match

## Markdown
- PRs on module for strikethrough, tables etc.
- Footnotes extension

## Image handling
Images go ... somewhere, ref them in the body
Script puts them in correct Drupal folder? could just put them there directly
Do we care if image is added into Drupal file management?
Autocompletion would fix the things, so yeah:
- create image, place in correct folder
- create autocompletion that looks in that folder
Mega idea: show preview in autocompletion, could at least create command to open image from vim

## Autocompletion
- tags
- posts
- pages
- images
- series
