<?php

namespace Drupal\front_matter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\front_matter\FrontMatterEncoder;
use Drupal\front_matter\FrontMatterProcessor;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Zend\Diactoros\Response\TextResponse;

class FrontMatterController extends ControllerBase {

  /**
   * @var \Drupal\front_matter\FrontMatterEncoder
   */
  protected $encoder;

  /**
   * @var \Drupal\front_matter\FrontMatterProcessor
   */
  protected $processor;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * FrontMatterController constructor.
   *
   * @param \Drupal\front_matter\FrontMatterEncoder $encoder
   *   The encoder.
   * @param \Drupal\front_matter\FrontMatterProcessor $processor
   *   The processor.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(FrontMatterEncoder $encoder, FrontMatterProcessor $processor, EntityTypeManagerInterface $entity_type_manager) {
    $this->encoder = $encoder;
    $this->processor = $processor;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('front_matter.encoder'),
      $container->get('front_matter.processor'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Create an entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The http request.
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  public function createEntity(Request $request) {
    $content = $request->getContent();
    if (empty($content)) {
      return new TextResponse('Error, empty request.');
    }

    $decoded = $this->encoder->decode($content, 'front_matter');
    if (!$decoded) {
      return new TextResponse('Error, bad request');
    }

    $created = $this->processor->create($decoded);
    if (!$created) {
      return new TextResponse('Error, couldn\'t create entity.');
    }

    return new TextResponse('Success! Created. [' . $created->id() . ']');
  }

  /**
   * Fetch entity by ID.
   *
   * @param string $id
   *   The entity ID.
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  public function fetchId(string $id) {
    $normalized = $this->processor->fetchById($id);
    if (!$normalized) {
      return new TextResponse('Error. Does not exist.');
    }

    $encoded = $this->encoder->encode($normalized, 'front_matter');
    return new TextResponse($encoded);
  }

  /**
   * Fetch entity by title.
   *
   * @param string $title
   *   The title of the entity.
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  public function fetchTitle(string $title) {
    $normalized = $this->processor->fetchByTitle($title);
    if (!$normalized) {
      return new TextResponse('Error. Does not exist.');
    }

    $encoded = $this->encoder->encode($normalized, 'front_matter');
    return new TextResponse($encoded);
  }

  /**
   * Get a list of posts.
   *
   * @param string $published
   *   Optionally filter by published
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  public function list(string $published = NULL) {
    $criteria = [
      'type' => 'blog_post',
    ];
    if ($published !== NULL) {
      $criteria['status'] = $published;
    }

    $list = $this->entityTypeManager->getStorage('node')->loadByProperties($criteria);
    $list = array_map(function ($item) {
      $published = $item->status->value == 0 ? '*' : '';
      return $item->title->value . $published . ' [' . $item->id() . ']';
    }, $list);

    return new TextResponse(implode(PHP_EOL, $list));
  }

  /**
   * Return a list of tags.
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  public function tags() {
    return $this->entityList('taxonomy_term', 'name', ['vid' => 'tags']);
  }

  /**
   * Return a list of series.
   */
  public function series() {
    return $this->entityList('node', 'title', ['type' => 'series']);
  }

  /**
   * Update an entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The http request.
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  public function update(Request $request) {
    $content = $request->getContent();
    if (empty($content)) {
      return new TextResponse('Error, empty request.');
    }

    $decoded = $this->encoder->decode($content, 'front_matter');
    if (!$decoded || empty($decoded)) {
      return new TextResponse('Error, bad request');
    }

    $updated = $this->processor->update($decoded);
    if (!$updated) {
      return new TextResponse('Error, couldn\'t update entity.');
    }

    return new TextResponse('Success! Updated. [' . $updated->id() . ']');
  }

  /**
   * Either create or update via a post.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The http request.
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  public function post(Request $request) {
    $content = $request->getContent();
    if (empty($content)) {
      return new TextResponse('Error, empty request.');
    }

    $decoded = $this->encoder->decode($content, 'front_matter');
    if (!$decoded || empty($decoded)) {
      return new TextResponse('Error, bad request');
    }

    if (empty($decoded['front_matter']['id!'])) {
      return $this->createEntity($request);
    }

    return $this->update($request);
  }

  /**
   * Convert a list of entities to a TextResponse list
   *
   * @param string $storage_type
   *   The entity storage type
   * @param string $field_id
   *   The ID of the field to use for the list item description
   * @param array $properties
   *   An array of properties to use to load the entities
   *
   * @return \Zend\Diactoros\Response\TextResponse
   */
  protected function entityList(string $storage_type, string $field_id, array $properties = []) {
    $terms = $this->entityTypeManager->getStorage($storage_type)->loadByProperties($properties);

    $terms = array_map(function ($item) use ($field_id) {
      $name = $item->{$field_id}->value;
      // Wrap in quotes if needed.
      if ((stristr($name, ',') !== FALSE || stristr($name, ' ') !== FALSE) && (stristr($name, '"') === FALSE)) {
        return '"' . $name . '"' . ' [' . $item->id() . ']';
      }

      return $name . ' [' . $item->id() . ']';
    }, $terms);

    return new TextResponse(implode(PHP_EOL, $terms));
  }

}
