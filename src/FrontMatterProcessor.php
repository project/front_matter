<?php

namespace Drupal\front_matter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Processor for front matter arrays.
 */
class FrontMatterProcessor {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Create a new post.
   *
   * @param array $content
   *   The content array.
   *
   * @return NodeInterface
   */
  public function create(array $content) {
    // @TODO Checking on fields
    // Images? maybe could query for them? Ideally
    // Series
    $front_matter = $content['front_matter'];
    $data = [
      'type' => 'blog_post',
      'langcode' => 'en',
      'status' => 0,
      'title' => 'temp111',
    ];
    $node = $this->entityTypeManager->getStorage('node')->create($data);
    $this->setNode($node, $front_matter, $content['content']);

    $node->save();

    return $node;
  }

  /**
   * Update a post.
   *
   * @param array $content
   *   The content array.
   *
   * @return NodeInterface
   */
  public function update(array $content) {
    // Check revisions working?
    $front_matter = $content['front_matter'];
    if (empty($front_matter['id!'])) {
      return FALSE;
    }

    $node = $this->entityTypeManager->getStorage('node')->load($front_matter['id!']);
    if (!$node) {
      return FALSE;
    }

    $this->setNode($node, $front_matter, $content['content']);
    $node->save();

    return $node;
  }

  /**
   * Fetch a post by ID.
   *
   * @param string $id
   *   The ID to use.
   */
  public function fetchById(string $id) {
    $node = $this->entityTypeManager->getStorage('node')->load($id);
    if ($node) {
      return $this->normalize($node);
    }

    return FALSE;
  }

  /**
   * Fetch a post by title.
   *
   * @param string $title
   *   The post title to try to match.
   */
  public function fetchByTitle(string $title) {
    $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties(['title' => $title]);
    if (!empty($nodes)) {
      return $this->normalize(reset($nodes));
    }

    return FALSE;
  }

  /**
   * Normalize an entity into an array.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to normalize.
   *
   * @return array
   */
  public function normalize(EntityInterface $entity) {
    $tags = [];
    foreach ($entity->field_blog_tags as $tag) {
      if (!empty($tag->entity->name->value)) {
        $tags[] = $tag->entity->name->value;
      }
    }

    $categories = [];
    foreach ($entity->field_category as $category) {
      if (!empty($category->entity->name->value)) {
        $categories[] = $category->entity->name->value;
      }
    }

    $created = DrupalDateTime::createFromTimestamp($entity->getCreatedTime())->format('Y-m-d H:i:s');

    $published_at = '';
    if (!empty($entity->published_at->value)) {
      $published_at = DrupalDateTime::createFromTimestamp($entity->published_at->value);
      $published_at = $published_at->format('Y-m-d');
    }
    $path = $entity->toUrl('canonical', [])->toString();
    $front_matter = [
      'title' => $entity->title->value,
      'date' => $published_at,
      'created!' => $created,
      'size!' => $this->getEntityValueId($entity->field_size->entity),
      'tags' => FrontMatterHelper::joinTags($tags),
      'category' => FrontMatterHelper::joinTags($categories),
      'series' => $this->getEntityValueId($entity->field_series->entity),
      'published' => $entity->status->value == '1' ? 'true' : 'false',
      'summary' => $entity->body->summary,
      'id!' => $entity->id(),
      'path!' => $path,
    ];
    $ret = [
      'front_matter' => $front_matter,
      'content' => $entity->body->value,
    ];

    return $ret;
  }

  /**
   * Get an entity value with an ID
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   (optional) The entity
   *
   * @return string
   *   The entity value ID e.g. Thing [1]
   */
  protected function getEntityValueId(EntityInterface $entity = NULL): string {
    if ($entity === NULL) {
      return '';
    }

    $title = $entity->title->value;
    if (empty($title) && isset($entity->name)) {
      $title = $entity->name->value;
    }

    return $title . ' [' . $entity->id() . ']';
  }

  /**
   * Set the fields.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The post node.
   * @param array $front_matter
   *   The front matter array.
   * @param string $content
   *   The body content.
   */
  protected function setNode(NodeInterface &$node, array $front_matter, string $content) {
    $node->title->value = $front_matter['title'];
    $published = $front_matter['published'] ?? '';
    $published = $published == 'true' ? 1 : 0;
    $node->status = $published;
    $node->body->value = $content;
    $node->body->format = 'markdown';
    $node->body->summary = $front_matter['summary'] ?? '';

    // Refs here.
    // Set references.
    $term_id = NULL;
    $this->clearRefField($node, 'field_blog_tags');
    $tags = $front_matter['tags'];
    foreach ($tags as $tag) {
      $term_id = $this->setTaxonomyRefField($node, 'field_blog_tags', $tag, 'tags');
    }

    $categories = $front_matter['category'];
    $this->clearRefField($node, 'field_category');
    foreach ($categories as $category) {
      $term_id = $this->setTaxonomyRefField($node, 'field_category', $category, 'blog_categories');
    }

    $size = $front_matter['size'] ?? '';
    if (empty($size) || $size == 'auto') {
      $size = $this->getSize($content);
    }
    $term_id = $this->setTaxonomyRefField($node, 'field_size', $size, 'post_size');

    $published_at = DrupalDateTime::createFromDateTime(new \DateTime());
    if (!empty($front_matter['date'])) {
      $published_at = DrupalDateTime::createFromFormat('Y-m-d', $front_matter['date']);
    }
    $node->published_at->value = $published_at->format('U');
  }

  /**
   * Get the size of the content as a size string.
   *
   * @param string $content
   *   The content to measure.
   */
  protected function getSize(string $content) {
    // @TODO pull this from config.
    $sizes = ['bite', 'snack', 'meal', 'banquet'];
    return FrontMatterHelper::mapSizeRange($content, $sizes);
  }

  /**
   * Clear a entity reference field.
   *
   * @param EntityInterface $entity
   *   The entity.
   * @param string $field_id
   *   The ID of the field to clear.
   */
  protected function clearRefField(EntityInterface &$entity, string $field_id) {
    $entity->{$field_id}->filter(function () { return FALSE; });
  }

  /**
   * Set a taxonomy reference on an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to update.
   * @param string $field_id
   *   The ID of the entity reference field.
   * @param string $term_name
   *   The taxonomy term name.
   * @param string $vocabulary_id
   *   The ID of the vocabulary.
   */
  protected function setTaxonomyRefField(EntityInterface &$entity, string $field_id, string $term_name, string $vocabulary_id) {
    // Check if already in field_id
    $referenced = $entity->get($field_id)->referencedEntities();
    foreach ($referenced as $ref) {
      if ($ref->name->value == $term_name) {
        return;
      }
    }

    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
      ->condition('vid', $vocabulary_id)
      ->condition('name', $term_name);
    $result = $query->execute();

    $term_id = NULL;
    if (count($result) > 0) {
      $term_id = reset($result);
      $entity->get($field_id)->appendItem(['target_id' => $term_id]);

      return $term_id;
    }

    $term = $this->entityTypeManager->getStorage('taxonomy_term')->create([
      'vid' => $vocabulary_id,
      'name' => $term_name,
    ]);
    $term->save();
    $term_id = $term->id();

    $entity->get($field_id)->appendItem(['target_id' => $term_id]);

    return $term_id;
  }

}
