<?php

namespace Drupal\front_matter;

use Drupal\front_matter\FrontMatterHelper;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * Encoder for front matter strings / arrays.
 */
class FrontMatterEncoder implements DecoderInterface, EncoderInterface {

  /**
   * The formats that this Encoder supports.
   *
   * @var array
   */
  protected static $format = [
    'front_matter',
  ];

  /**
   * {@inheritdoc}
   */
  public function supportsEncoding($format) {
    return in_array($format, static::$format);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDecoding($format) {
    return in_array($format, static::$format);
  }

  /**
   * {@inheritdoc}
   */
  public function encode($data, $format, array $context = []) {
    if (!array_key_exists('front_matter', $data)) {
      return FALSE;
    }

    if (!array_key_exists('content', $data)) {
      return FALSE;
    }

    $output = '---' . PHP_EOL;
    foreach ($data['front_matter'] as $field_name => $value) {
      $output .= $field_name . ': ' . $value . PHP_EOL;
    }
    $output .= '---' . PHP_EOL;

    $output .= $data['content'];

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function decode($data, $format, array $context = []) {
    $front_matter = $this->getFrontMatter($data);
    $content = $this->getContent($data);
    return [
      'front_matter' => $front_matter,
      'content' => $content,
    ];
  }

  /**
   * Get the front matter array from a string
   *
   * @param string $data
   *   The data
   *
   * @return bool|array
   *   Either FALSE or the front matter array
   */
  public function getFrontMatter(string $data) {
    $sections = $this->getSections($data);
    if (count($sections) != 2) {
      return FALSE;
    }

    $section = $sections[0];
    // Split into fields
    $fields = [];
    foreach ($section as $line) {
      preg_match('/(^.*):\s?(.*)$/', $line, $matches);
      if (count($matches) == 3) {
        $fields[strtolower($matches[1])] = $matches[2];
      }
    }
    if (empty($fields)) {
      return FALSE;
    }

    $tagged_fields = ['tags', 'category'];
    foreach ($tagged_fields as $tagged_field) {
      if (isset($fields[$tagged_field])) {
        $fields[$tagged_field] = FrontMatterHelper::splitTags($fields[$tagged_field]);
      }
    }

    return $fields;
  }

  /**
   * Get the content section from a string
   *
   * @param string $data
   *   The data string
   *
   * @return bool|string
   *   Either FALSE or the content section
   */
  public function getContent(string $data) {
    $sections = $this->getSections($data);
    if (count($sections) != 2) {
      return FALSE;
    }

    return implode(PHP_EOL, $sections[1]);
  }

  /**
   * Get the sections defined by --- markers
   *
   * @param string $data
   *   The data string
   *
   * @return array
   *   The sections
   */
  public function getSections(string $data) {
    $lines = preg_split("/\r\n|\n|\r/", $data);

    $sections = [];
    $current_section = 0;
    foreach ($lines as $line) {
      if (preg_match('/^---/', $line) === 1) {
        if (!empty($sections)) {
          $current_section++;
        }
        continue;
      }

      $sections[$current_section][] = $line;
    }

    // Remove empty sections
    $filtered_sections = [];
    foreach ($sections as $section) {
      $notEmpty = FALSE;
      foreach ($section as $line) {
        $notEmpty = $notEmpty || !empty($line);
      }
      if ($notEmpty) {
        $filtered_sections[] = $section;
      }
    }

    return $filtered_sections;
  }
}
