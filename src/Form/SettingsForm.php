<?php

namespace Drupal\front_matter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure front matter settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'front_matter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'front_matter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('front_matter.settings');

    $form['node_types'] = [
      '#type' => 'details',
      '#title' => $this->t('Node types'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];

    // @TODO fill this out!

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $settings = $form_state->cleanValues()->getValues();
    $this->config('front_matter.settings')
      ->setData($settings)
      ->save();
  }

}
