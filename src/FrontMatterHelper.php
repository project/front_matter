<?php

namespace Drupal\front_matter;

/**
 * Front matter helper class.
 */
class FrontMatterHelper {

  /**
   * Split a string of tags into an array.
   *
   * @param string $tags
   *   The string of tags to split.
   *
   * @return array
   */
  public static function splitTags(string $tags): array {
    if (empty($tags)) {
      return [];
    }

    $split = str_getcsv($tags);
    $split = array_map('trim', $split);
    return $split;
  }

  /**
   * Join an array of tags into a string.
   *
   * @param array $tags
   *   The array of tags to join.
   *
   * @return string
   */
  public static function joinTags(array $tags): string {
    if (empty($tags)) {
      return '';
    }

    $memory = fopen('php://memory', 'r+');
    if (fputcsv($memory, $tags) === FALSE) {
        return '';
    }
    rewind($memory);
    $join = stream_get_contents($memory);
    return rtrim($join);
  }

  /**
   * Map the size of a string to an array of choices.
   *
   * @param string $content
   *   The content to measure
   * @param array $sizes
   *   The array of sizes
   *
   * @return string The chosen size
   */
  public static function mapSizeRange(string $content, array $sizes) {
    if (empty($content)) {
      return $sizes[0];
    }

    $chars = 300;
    $mul = 2;
    for ($level = 0; $level < count($sizes) - 1; $level++) {
      if (strlen($content) <= $chars) {
        return $sizes[$level];
      }
      $chars *= $mul;
    }

    return end($sizes);
  }

  /**
   * Get an ID from a string
   *
   * @param string $string
   *   The string to use
   *
   * @return null|int The ID or null
   */
  public static function getIdFromString(string $string) {
    preg_match('/.*\[(\d+)\]$/', $string, $match);
    if (!empty($match[1])) {
      return $match[1];
    }

    return NULL;
  }

}
