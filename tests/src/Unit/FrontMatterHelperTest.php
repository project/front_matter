<?php

namespace Drupal\Tests\front_matter;

use Drupal\Tests\UnitTestCase;
use Drupal\front_matter\FrontMatterHelper;

/**
 * FrontMatterHelper testing.
 */
class FrontMatterHelperTest extends UnitTestCase {

  /**
   * Test the splitTags method.
   */
  public function testSplitTags() {
    $result = FrontMatterHelper::splitTags('tags');
    $this->assertCount(1, $result);
    $this->assertEquals('tags', $result[0]);

    $result = FrontMatterHelper::splitTags('tags, another');
    $this->assertCount(2, $result);
    $this->assertEquals('tags', $result[0]);
    $this->assertEquals('another', $result[1]);

    $result = FrontMatterHelper::splitTags('tags, another, "blah, blah"');
    $this->assertCount(3, $result);
    $this->assertEquals('blah, blah', $result[2]);
  }

  /**
   * Test the joinTags method.
   */
  public function testJoinTags() {
    $result = FrontMatterHelper::joinTags(['tags']);
    $this->assertEquals('tags', $result);

    $result = FrontMatterHelper::joinTags(['tags', 'another']);
    $this->assertEquals('tags,another', $result);

    $result = FrontMatterHelper::joinTags(['tags', 'another', 'blah, blah']);
    $this->assertEquals('tags,another,"blah, blah"', $result);
  }

  public function testMapSizeRange() {
    $sizes = ['bite', 'snack', 'meal', 'banquet'];
    $result = FrontMatterHelper::mapSizeRange('', $sizes);
    $this->assertEquals('bite', $result);

    $result = FrontMatterHelper::mapSizeRange($this->generateContent(200), $sizes);
    $this->assertEquals('bite', $result);

    $result = FrontMatterHelper::mapSizeRange($this->generateContent(400), $sizes);
    $this->assertEquals('snack', $result);

    $result = FrontMatterHelper::mapSizeRange($this->generateContent(600), $sizes);
    $this->assertEquals('snack', $result);

    $result = FrontMatterHelper::mapSizeRange($this->generateContent(601), $sizes);
    $this->assertEquals('meal', $result);

    $result = FrontMatterHelper::mapSizeRange($this->generateContent(1199), $sizes);
    $this->assertEquals('meal', $result);

    $result = FrontMatterHelper::mapSizeRange($this->generateContent(1201), $sizes);
    $this->assertEquals('banquet', $result);

    $result = FrontMatterHelper::mapSizeRange($this->generateContent(2001), $sizes);
    $this->assertEquals('banquet', $result);
  }

  public function testGetIdFromString() {
    $result = FrontMatterHelper::getIdFromString('thing');
    $this->assertNull($result);

    $result = FrontMatterHelper::getIdFromString('thing [1]');
    $this->assertEquals(1, $result);

    $result = FrontMatterHelper::getIdFromString('thing 1');
    $this->assertNull($result);

    $result = FrontMatterHelper::getIdFromString('thing []');
    $this->assertNull($result);

    $result = FrontMatterHelper::getIdFromString('"thing [1]"');
    $this->assertNull($result);

    $result = FrontMatterHelper::getIdFromString('"thing [1]" [22]');
    $this->assertEquals(22, $result);
  }

  protected function generateContent($length) {
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    $str = '';
    for ($i = 0; $i < $length; $i++) {
      $shuffled = str_shuffle($permitted_chars);
      $str .= $shuffled[0];
    }

    return $str;
  }
}
