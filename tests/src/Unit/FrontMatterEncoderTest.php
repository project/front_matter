<?php

namespace Drupal\Tests\front_matter;

use Drupal\Tests\UnitTestCase;
use Drupal\front_matter\FrontMatterEncoder;

/**
 * Serializer test.
 *
 * @group front_matter
 */
class FrontMatterEncoderTest extends UnitTestCase {

  /**
   * The encoder under test.
   *
   * @var \Drupal\front_matter\FrontMatterEncoder;
   */
  protected $encoder;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->encoder = new FrontMatterEncoder();
  }

  /**
   * Test the getSections method.
   */
  public function testGetSections() {
    $sections = $this->encoder->getSections("---\r\nHey");
    $this->assertCount(1, $sections);
    $this->assertCount(1, $sections[0]);

    $sections = $this->encoder->getSections("Hey");
    $this->assertCount(1, $sections);
    $this->assertCount(1, $sections[0]);

    $sections = $this->encoder->getSections("---");
    $this->assertCount(0, $sections);

    $sections = $this->encoder->getSections("---\r\n");
    $this->assertCount(0, $sections);

    $sections = $this->encoder->getSections("---\r\nHey\r\nHello");
    $this->assertCount(1, $sections);
    $this->assertCount(2, $sections[0]);

    $sections = $this->encoder->getSections("---\r\nHey\r\nHello\r\n---\r\nBody");
    $this->assertCount(2, $sections);
    $this->assertCount(2, $sections[0]);
    $this->assertCount(1, $sections[1]);

    $sections = $this->encoder->getSections("\r\n\r\n---\r\nHey\r\n---\r\nBody");
    $this->assertCount(2, $sections);
    $this->assertCount(1, $sections[0]);
    $this->assertCount(1, $sections[1]);

    $data = <<<EOT
---
title: Extreme value delivery
date: 2019-05-14
---
## Legendary
Recently I watched [Gary Bernhardt's talk](https://www.deconstructconf.com/2018/gary-bernhardt-dougs-demo) about the
EOT;

    $sections = $this->encoder->getSections($data);
    $this->assertCount(2, $sections);
    $this->assertCount(2, $sections[0]);
    $this->assertCount(2, $sections[1]);
  }

  /**
   * Test the decode method.
   */
  public function testDecode() {
    $data = <<<EOT
---
title: Extreme value delivery
date: 2019-05-14
---
## Legendary
EOT;
    $front_matter = $this->encoder->getFrontMatter($data);
    $this->assertCount(2, $front_matter);
    $this->assertArrayHasKey('title', $front_matter);
    $this->assertArrayHasKey('date', $front_matter);

    $content = $this->encoder->getContent($data);
    $this->assertEquals('## Legendary', $content);

    $decode = $this->encoder->decode($data, 'front_matter');
    $this->assertCount(2, $decode);
    $this->assertCount(2, $decode['front_matter']);
    $this->assertEquals('## Legendary', $decode['content']);
  }

  public function testEncode() {
    $data = <<<EOT
---
title: Extreme value delivery
date: 2019-05-14
---
## Legendary
EOT;
    $decoded = $this->encoder->decode($data, 'front_matter');
    $encoded = $this->encoder->encode($decoded, 'front_matter');
    $this->assertEquals($data, $encoded);
  }

}
